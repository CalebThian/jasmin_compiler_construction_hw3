/* Definition section */
%{
    #include "common.h"
    #include "y.tab.h"	/* header file generated by bison */
    extern YYSTYPE yylval;
    extern int scope_lev;
    extern int ss_size;
    extern int debug_mode;
    #define YY_NO_UNPUT
    #define YY_NO_INPUT
/*  
    #define INC 267
    #define DEC 268
    #define GEQ 269
    #define LEQ 270
    #define EQL 271
    #define NEQ 272
    #define ADD_ASSIGN 273
    #define SUB_ASSIGN 274
    #define MUL_ASSIGN 275
    #define QUO_ASSIGN 276
    #define REM_ASSIGN 277
    #define LAND 278
    #define LOR 279
    #define NEWLINE 280
    #define PRINT 281
    #define PRINTLN 282
    #define IF 283
    #define ELSE 284
    #define FOR 285
*/
%}

/* Define regular expression label */
letter [a-zA-Z_]
digit [0-9]
id {letter}+({letter}|{digit})*
inumber {digit}+
fnumber ({digit}*\.{digit}+)

%x CMT
%x STR
%option yylineno

/* Rules section */
%%

"/*"        { BEGIN(CMT); }
<CMT>"*/"   { BEGIN(INITIAL); }
<CMT>\n     {;}
<CMT>.      {;}
"//".*      {;}
"\""        { 
                BEGIN(STR);
                return QUOTA;
            }
<STR>"\""   { BEGIN(INITIAL);
                return QUOTA;
            }
<STR>[^"\""]* { 
                yylval.s_val = strdup(yytext);
                return STRING_LIT;
            }
"+"         { return '+'; }
"-"         { return '-'; }
"*"         { return '*'; }
"/"         { return '/'; }
"%"         { return '%'; }
"++"        { return INC; }
"--"        { return DEC; }

">"         { return '>'; }
"<"         { return '<'; }
">="        { return GEQ; }
"<="        { return LEQ; }
"=="        { return EQL; }
"!="        { return NEQ; }

"="         { return '='; }
"+="        { return ADD_ASSIGN; }
"-="        { return SUB_ASSIGN; }
"*="        { return MUL_ASSIGN; }
"/="        { return QUO_ASSIGN; }
"%="        { return REM_ASSIGN; }

"&&"        { return LAND; }
"||"        { return LOR; }
"!"         { return '!'; }

"("         { 
                ++ss_size;
                if(debug_mode==1) printf("++ss_size=%d\n",ss_size);
                return '('; 
            }
")"         { return ')'; }
"["         { 
               ++ss_size;
               if(debug_mode==1) printf("++ss_size=%d\n",ss_size);
                return '['; 
            }
"]"         { return ']'; }
"{"         { scope_lev++;return '{'; }
"}"         { return '}'; }

";"         { return ';'; }
","         { return ','; }
"\n"        { return NEWLINE; }

"print"     { return PRINT; }
"println"   { return PRINTLN; }
"if"        { return IF; }
"else"      { return ELSE; }
"for"       { return FOR; }

"int32"     { return INT; }
"float32"   { return FLOAT;}
"string"    { return STRING; }
"bool"      { return BOOL; }
"true"      { yylval.i_val=1; return BOOL_LIT;}
"false"     { yylval.i_val=0; return BOOL_LIT;}
"var"       { return VAR; }

{inumber}   { 
                yylval.i_val = atoi(yytext);
                return INT_LIT;
            }
{fnumber}   { 
                yylval.f_val = atof(yytext);
	    	    return FLOAT_LIT; 
	        }
{id}        { 
                yylval.s_val=strdup(yytext);
	    	    return IDENT;
	        }

<<EOF>>     { static int once = 0;
                if (once++) {
                    yyterminate();
                }
                yylineno++;
                return NEWLINE;
            }
[ \t]+      {;}
.           {;}
%%

/*  C Code section */
int yywrap(void)
{
    return 1;
}
