/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 2 "compiler_hw3.y" /* yacc.c:339  */

    #include "common.h" //Extern variables that communicate with lex
    #define MAXSTACK 1000
    // #define YYDEBUG 1
    // int yydebug = 1;

    extern int yylineno;
    extern int yylex();
    extern FILE *yyin;
    extern char* yytext; 
    void yyerror (char const *s)
    {
        printf("error:%d: %s\n", yylineno, s);
    }

    /* Symbol table function - you can add new function if needed. */
    static void create_symbol();
    static void insert_symbol(char* a,int scope_lev);
    static int lookup_symbol(char* a,int scope_lev);
    static int check_symbol(char* a,int scope_lev);
    static void dump_symbol();
    static int addr_symbol(char* a);
    static char* type_symbol(char* a);
    static char* ele_type_symbol(char* a);
    static void push(char* s);
    static char* pop();
    static void pop1(int a);
    static void popAll();
    static void checkTable();
    static void pushv(char*s);
    static char* popv();
    static char* strlower();

    /*global variable*/
    int ind=0;
    int addr=0;
    int scope_lev=0;
    symbol symTable[10000];
    char* ytype;
    int sign_size=0;
    int declare=0;
    char* stack[MAXSTACK];
    int s_stack[MAXSTACK];//scope of operator
    int ss_size;//temp of element of s_stack
    int stack_size=0;
    char* id_name;
    char* expType;
    int array=0;
    char* id_type;
    char* ele;
    int sorl=0;//store or load
    int saddr=0;//use for inc and dec
    int labelNo=0;//use for jump
    int ifNo=0;
    int ifNow[MAXSTACK];
    int ifLevel=0;
    int ffor=0;//flag of for
    int forNo=0;
    int forNow[MAXSTACK];
    int forLevel=0;
//    char* lastBool=0;//use for !
    char* assign_id_type;//assign-statement-use id
    int assign_array=0;//assign-statement-use array

    char* stackv[MAXSTACK];
    int stackv_size=0;
    int debug_mode=0;
    
    FILE *fp;
    int HAS_ERROR=0;

#line 138 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    VAR = 258,
    INT = 259,
    FLOAT = 260,
    BOOL = 261,
    STRING = 262,
    INC = 263,
    DEC = 264,
    GEQ = 265,
    LEQ = 266,
    EQL = 267,
    NEQ = 268,
    ADD_ASSIGN = 269,
    SUB_ASSIGN = 270,
    MUL_ASSIGN = 271,
    QUO_ASSIGN = 272,
    REM_ASSIGN = 273,
    LAND = 274,
    LOR = 275,
    NEWLINE = 276,
    PRINT = 277,
    PRINTLN = 278,
    IF = 279,
    ELSE = 280,
    FOR = 281,
    QUOTA = 282,
    INT_LIT = 283,
    FLOAT_LIT = 284,
    IDENT = 285,
    STRING_LIT = 286,
    BOOL_LIT = 287
  };
#endif
/* Tokens.  */
#define VAR 258
#define INT 259
#define FLOAT 260
#define BOOL 261
#define STRING 262
#define INC 263
#define DEC 264
#define GEQ 265
#define LEQ 266
#define EQL 267
#define NEQ 268
#define ADD_ASSIGN 269
#define SUB_ASSIGN 270
#define MUL_ASSIGN 271
#define QUO_ASSIGN 272
#define REM_ASSIGN 273
#define LAND 274
#define LOR 275
#define NEWLINE 276
#define PRINT 277
#define PRINTLN 278
#define IF 279
#define ELSE 280
#define FOR 281
#define QUOTA 282
#define INT_LIT 283
#define FLOAT_LIT 284
#define IDENT 285
#define STRING_LIT 286
#define BOOL_LIT 287

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 79 "compiler_hw3.y" /* yacc.c:355  */

    int i_val;
    float f_val;
    char *s_val;
    struct lr_val{
        char* l_val;
        char* r_val;
    }
    /* ... */

#line 253 "y.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 270 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  61
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   245

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  49
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  59
/* YYNRULES -- Number of rules.  */
#define YYNRULES  110
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  159

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   287

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    42,     2,     2,     2,    41,     2,     2,
      43,    44,    39,    37,     2,    38,     2,    40,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    48,
      35,    45,    36,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    33,     2,    34,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    46,     2,    47,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   121,   121,   125,   126,   130,   131,   135,   137,   139,
     141,   146,   156,   157,   159,   164,   165,   167,   171,   172,
     172,   174,   178,   179,   180,   184,   185,   189,   190,   195,
     197,   199,   201,   203,   205,   210,   212,   215,   217,   222,
     227,   229,   231,   235,   236,   239,   240,   241,   245,   246,
     247,   256,   265,   274,   284,   294,   308,   322,   323,   324,
     325,   326,   327,   328,   332,   333,   334,   338,   348,   356,
     360,   383,   386,   386,   394,   396,   398,   400,   402,   404,
     408,   410,   418,   428,   431,   433,   435,   439,   441,   443,
     446,   454,   460,   461,   461,   464,   471,   472,   471,   475,
     476,   477,   478,   480,   486,   493,   493,   493,   515,   515,
     515
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "VAR", "INT", "FLOAT", "BOOL", "STRING",
  "INC", "DEC", "GEQ", "LEQ", "EQL", "NEQ", "ADD_ASSIGN", "SUB_ASSIGN",
  "MUL_ASSIGN", "QUO_ASSIGN", "REM_ASSIGN", "LAND", "LOR", "NEWLINE",
  "PRINT", "PRINTLN", "IF", "ELSE", "FOR", "QUOTA", "INT_LIT", "FLOAT_LIT",
  "IDENT", "STRING_LIT", "BOOL_LIT", "'['", "']'", "'<'", "'>'", "'+'",
  "'-'", "'*'", "'/'", "'%'", "'!'", "'('", "')'", "'='", "'{'", "'}'",
  "';'", "$accept", "Program", "StatementList", "Type", "TypeName",
  "ArrayType", "Expression", "Term1", "Term2", "$@1", "Term3", "Term4",
  "UnaryExpr", "cmp_op", "add_op", "mul_op", "unary_op", "LAND_op",
  "LOR_op", "PrimaryExpr", "Operand", "Literal", "IndexExpr",
  "ConversionExpr", "Statement", "SimpleStmt", "DeclarationStmt",
  "Declaration", "identifier", "identifier_dec", "AssignmentStmt", "$@2",
  "assign_op", "ExpressionStmt", "IncDecStmt", "Block", "IfStmt", "If",
  "Else", "Condition_if", "Condition", "Block_if", "ForStmt", "$@3", "For",
  "ForClause", "$@4", "$@5", "InitStmt", "Condition_for",
  "Condition_for_c", "PostStmt", "Block_for", "Block_for_c", "PrintStmt",
  "$@6", "$@7", "$@8", "$@9", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,    91,    93,    60,    62,    43,    45,    42,
      47,    37,    33,    40,    41,    61,   123,   125,    59
};
# endif

#define YYPACT_NINF -89

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-89)))

#define YYTABLE_NINF -92

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     117,   -15,   -89,   -89,   -89,   -89,   -89,   -11,    -2,   -89,
     -89,    21,   -89,   -89,   -89,   -89,   160,   -89,   -89,   -89,
     160,   117,    53,   117,    14,   -89,   -89,     3,    39,    50,
       1,    41,   172,   160,    36,   -89,   -89,   -89,   -89,   -89,
      51,    62,    26,   -89,   -89,   -89,   -89,    63,    66,   160,
      69,   160,    70,   -89,    12,   -89,   -89,    47,     2,   -13,
      72,   -89,   -89,   160,   -89,   -89,   -89,   160,    11,   -89,
     160,   -89,   -89,   -89,   -89,   -89,   -89,   -89,   -89,   -89,
     160,   -89,   -89,   -89,   160,   -89,   160,   -89,   -89,   160,
     -89,   -89,    87,    65,   -89,   -89,     0,   -89,   -89,   -89,
      60,    65,   -89,   -89,   160,   160,   -89,    12,   -89,   -89,
     -10,    39,   204,   -89,   -89,   -89,   -89,   -89,   -89,   160,
      50,    27,   160,    41,   -89,   -89,    10,    87,    88,    91,
      65,   -89,   -89,   -89,    87,    87,   -89,   -89,    87,     1,
      86,   -89,   -89,    -3,   -89,   -89,   160,    68,    73,   -89,
     -89,   -89,    80,   -89,   -89,   -89,   160,   -89,   -89
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     7,     8,    10,     9,    63,     0,     0,    87,
      95,     0,    51,    52,    70,    53,     0,    40,    41,    42,
       0,     0,     0,     2,     0,     5,     6,    72,    14,    17,
      21,    24,    12,     0,    27,    45,    48,    46,    47,     4,
       0,     0,    67,    49,    64,    65,    66,     0,     0,     0,
       0,     0,     0,    71,     0,   105,   108,     0,     0,     0,
       0,     1,     3,     0,    81,    82,    44,     0,     0,    43,
       0,    34,    32,    29,    30,    31,    33,    19,    35,    36,
       0,    37,    38,    39,     0,    28,     0,    58,    57,     0,
      59,    60,    90,     0,    89,    61,    72,    99,   100,    93,
       0,     0,    62,    69,     0,     0,    54,     0,    50,    83,
       0,    13,    15,    75,    76,    77,    78,    79,    74,     0,
      16,    18,     0,    23,    25,    26,     0,    68,    84,     0,
       0,    96,   103,    92,   106,   109,    11,    56,    73,    20,
      22,    55,    88,     0,   104,    94,     0,     0,     0,    86,
      85,   101,     0,   107,   110,    97,     0,   102,    98
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -89,   -89,   108,   -48,   -89,   -89,   -16,    64,    67,   -89,
      13,    52,   -30,   -89,   -89,   -89,   -89,   -89,   -89,   -89,
     -89,   -89,   -89,   -89,    -9,   -50,   -89,   -89,   -89,   -89,
     -89,   -89,   -89,   -89,   -89,   -88,    -7,   -89,   -89,   -89,
     -49,   -89,   -89,   -89,   -89,   -89,   -89,   -89,   -89,   -89,
     -89,   -89,   -89,   -89,   -89,   -89,   -89,   -89,   -89
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    22,    23,    24,    25,    26,    27,    28,    29,   122,
      30,    31,    32,    77,    80,    84,    33,    70,    67,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    54,
      44,    68,   119,    45,    46,    47,    48,    49,   143,    93,
      94,   129,    50,   130,    51,    99,   146,   156,   100,   101,
     152,   158,   133,   145,    52,   104,   147,   105,   148
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      58,    97,    98,    85,    59,   128,   103,    66,    64,    65,
      66,    64,    65,   132,    62,    53,     2,     3,     4,     5,
      66,     9,    66,    66,   -80,   113,   114,   115,   116,   117,
      66,   108,    55,    92,   137,    96,   107,   112,    78,    79,
     121,    56,   144,    21,   141,    16,   -90,   110,   -80,   -80,
     124,    62,    57,    61,   125,   149,   118,    63,    69,   136,
      71,    72,    73,    74,   -22,   -22,   -25,   -25,   -25,    86,
     126,    89,    87,   127,   106,     1,     2,     3,     4,     5,
      81,    82,    83,    88,    90,    75,    76,    91,   134,   135,
      95,   102,   140,     6,     7,     8,     9,   151,    10,    11,
      12,    13,    14,   138,    15,    16,   157,    66,   131,    17,
      18,    21,   153,   -91,    19,    20,   142,   154,    21,   109,
       1,     2,     3,     4,     5,   -25,   -25,   -25,   155,    60,
      92,   111,   123,     0,     0,   139,   150,   120,     6,     7,
       8,     9,     0,    10,    11,    12,    13,    14,     0,    15,
      16,     0,     0,     0,    17,    18,     0,     0,     0,    19,
      20,     0,     0,    21,     2,     3,     4,     5,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   -18,   -18,   -18,   -18,     0,    11,    12,    13,
      14,   -15,    15,    16,     0,     0,     0,    17,    18,     0,
       0,     0,    19,    20,     0,     0,     0,   -18,   -18,   -22,
     -22,   -25,   -25,   -25,   -18,   -18,   -18,   -18,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   -18,
     -18,   -22,   -22,   -25,   -25,   -25
};

static const yytype_int16 yycheck[] =
{
      16,    51,    51,    33,    20,    93,    54,    20,     8,     9,
      20,     8,     9,   101,    23,    30,     4,     5,     6,     7,
      20,    24,    20,    20,    21,    14,    15,    16,    17,    18,
      20,    44,    43,    49,    44,    51,    34,    67,    37,    38,
      70,    43,   130,    46,    34,    33,    46,    63,    48,    46,
      80,    60,    31,     0,    84,   143,    45,    43,    19,   107,
      10,    11,    12,    13,    37,    38,    39,    40,    41,    33,
      86,    45,    21,    89,    27,     3,     4,     5,     6,     7,
      39,    40,    41,    21,    21,    35,    36,    21,   104,   105,
      21,    21,   122,    21,    22,    23,    24,   146,    26,    27,
      28,    29,    30,   119,    32,    33,   156,    20,    48,    37,
      38,    46,    44,    25,    42,    43,    25,    44,    46,    47,
       3,     4,     5,     6,     7,    39,    40,    41,    48,    21,
     146,    67,    80,    -1,    -1,   122,   143,    70,    21,    22,
      23,    24,    -1,    26,    27,    28,    29,    30,    -1,    32,
      33,    -1,    -1,    -1,    37,    38,    -1,    -1,    -1,    42,
      43,    -1,    -1,    46,     4,     5,     6,     7,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    10,    11,    12,    13,    -1,    27,    28,    29,
      30,    19,    32,    33,    -1,    -1,    -1,    37,    38,    -1,
      -1,    -1,    42,    43,    -1,    -1,    -1,    35,    36,    37,
      38,    39,    40,    41,    10,    11,    12,    13,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    35,
      36,    37,    38,    39,    40,    41
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     6,     7,    21,    22,    23,    24,
      26,    27,    28,    29,    30,    32,    33,    37,    38,    42,
      43,    46,    50,    51,    52,    53,    54,    55,    56,    57,
      59,    60,    61,    65,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    79,    82,    83,    84,    85,    86,
      91,    93,   103,    30,    78,    43,    43,    31,    55,    55,
      51,     0,    73,    43,     8,     9,    20,    67,    80,    19,
      66,    10,    11,    12,    13,    35,    36,    62,    37,    38,
      63,    39,    40,    41,    64,    61,    33,    21,    21,    45,
      21,    21,    55,    88,    89,    21,    55,    74,    89,    94,
      97,    98,    21,    52,   104,   106,    27,    34,    44,    47,
      55,    56,    61,    14,    15,    16,    17,    18,    45,    81,
      57,    61,    58,    60,    61,    61,    55,    55,    84,    90,
      92,    48,    84,   101,    55,    55,    52,    44,    55,    59,
      61,    34,    25,    87,    84,   102,    95,   105,   107,    84,
      85,    89,    99,    44,    44,    48,    96,    74,   100
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    49,    50,    51,    51,    52,    52,    53,    53,    53,
      53,    54,    55,    55,    55,    56,    56,    56,    57,    58,
      57,    57,    59,    59,    59,    60,    60,    61,    61,    62,
      62,    62,    62,    62,    62,    63,    63,    64,    64,    64,
      65,    65,    65,    66,    67,    68,    68,    68,    69,    69,
      69,    70,    70,    70,    70,    71,    72,    73,    73,    73,
      73,    73,    73,    73,    74,    74,    74,    75,    75,    76,
      77,    78,    80,    79,    81,    81,    81,    81,    81,    81,
      82,    83,    83,    84,    85,    85,    85,    86,    87,    88,
      89,    90,    91,    92,    91,    93,    95,    96,    94,    97,
      98,    99,   100,   101,   102,   104,   105,   103,   106,   107,
     103
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     4,     1,     3,     1,     1,     3,     1,     1,     0,
       4,     1,     1,     3,     1,     1,     3,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     1,     1,     1,     3,     4,     4,     2,     2,     2,
       2,     2,     2,     1,     1,     1,     1,     1,     3,     3,
       1,     1,     0,     4,     1,     1,     1,     1,     1,     1,
       1,     2,     2,     3,     3,     5,     5,     1,     1,     1,
       1,     1,     3,     0,     4,     1,     0,     0,     7,     1,
       1,     1,     1,     1,     1,     0,     0,     6,     0,     0,
       6
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 7:
#line 136 "compiler_hw3.y" /* yacc.c:1646  */
    {ytype="int32";}
#line 1493 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 138 "compiler_hw3.y" /* yacc.c:1646  */
    {ytype="float32";}
#line 1499 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 140 "compiler_hw3.y" /* yacc.c:1646  */
    {ytype="string";}
#line 1505 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 142 "compiler_hw3.y" /* yacc.c:1646  */
    {ytype="bool";}
#line 1511 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 147 "compiler_hw3.y" /* yacc.c:1646  */
    {
            pop1(s_stack[ss_size]);
            --ss_size;
            if(debug_mode==1) printf("\t[arr_declare]:--ss_size=%d\n",ss_size);
            array=1;
        }
#line 1522 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 158 "compiler_hw3.y" /* yacc.c:1646  */
    {pop1(s_stack[ss_size]);expType="bool";}
#line 1528 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 160 "compiler_hw3.y" /* yacc.c:1646  */
    {pop1(s_stack[ss_size]);}
#line 1534 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 166 "compiler_hw3.y" /* yacc.c:1646  */
    {expType="bool";}
#line 1540 "y.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 172 "compiler_hw3.y" /* yacc.c:1646  */
    {if(ffor==1) fprintf(fp,"%cload %d\n",tolower(expType[0]=='a'?ele[0]:expType[0]),saddr);}
#line 1546 "y.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 173 "compiler_hw3.y" /* yacc.c:1646  */
    {expType="bool";}
#line 1552 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 185 "compiler_hw3.y" /* yacc.c:1646  */
    {pop1(s_stack[ss_size]);}
#line 1558 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 196 "compiler_hw3.y" /* yacc.c:1646  */
    {push("EQL");}
#line 1564 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 198 "compiler_hw3.y" /* yacc.c:1646  */
    {push("NEQ");}
#line 1570 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 200 "compiler_hw3.y" /* yacc.c:1646  */
    {push("LSS");}
#line 1576 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 202 "compiler_hw3.y" /* yacc.c:1646  */
    {push("LEQ");}
#line 1582 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 204 "compiler_hw3.y" /* yacc.c:1646  */
    {push("GTR");}
#line 1588 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 206 "compiler_hw3.y" /* yacc.c:1646  */
    {push("GEQ");}
#line 1594 "y.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 211 "compiler_hw3.y" /* yacc.c:1646  */
    {push("ADD");}
#line 1600 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 213 "compiler_hw3.y" /* yacc.c:1646  */
    {push("SUB");}
#line 1606 "y.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 216 "compiler_hw3.y" /* yacc.c:1646  */
    {push("MUL");}
#line 1612 "y.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 218 "compiler_hw3.y" /* yacc.c:1646  */
    {//push("QUO");
            //For jasmin, quo==>div
         push("DIV");
        }
#line 1621 "y.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 223 "compiler_hw3.y" /* yacc.c:1646  */
    {push("REM");}
#line 1627 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 228 "compiler_hw3.y" /* yacc.c:1646  */
    {push("POS");sign_size+=1;}
#line 1633 "y.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 230 "compiler_hw3.y" /* yacc.c:1646  */
    {push("NEG");sign_size+=1;}
#line 1639 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 232 "compiler_hw3.y" /* yacc.c:1646  */
    {push("NOT");sign_size+=1;}
#line 1645 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 235 "compiler_hw3.y" /* yacc.c:1646  */
    {push("LAND");}
#line 1651 "y.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 236 "compiler_hw3.y" /* yacc.c:1646  */
    {push("LOR");}
#line 1657 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 248 "compiler_hw3.y" /* yacc.c:1646  */
    {
            pop1(s_stack[ss_size]);
            --ss_size;
            if(debug_mode==1) printf("\t(exp):--ss_size=%d\n",ss_size);
        }
#line 1667 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 257 "compiler_hw3.y" /* yacc.c:1646  */
    {
            //printf("INT_LIT %d\n",yylval.i_val);
            fprintf(fp,"ldc %d\n",yylval.i_val);
            pushv("Lint32");
            pop1(sign_size);
            sign_size=0;
            expType="int32";
        }
#line 1680 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 266 "compiler_hw3.y" /* yacc.c:1646  */
    {
            //printf("FLOAT_LIT %d\n",yylval.i_val);
            fprintf(fp,"ldc %lf\n",yylval.f_val);
            pushv("Lfloat32");
            pop1(sign_size);
            sign_size=0;
            expType="float32";
        }
#line 1693 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 275 "compiler_hw3.y" /* yacc.c:1646  */
    {
            //printf("BOOL_LIT %d\n",yylval.i_val);
            fprintf(fp,"iconst_%d\n",yylval.i_val);
            //lastBool=(yylval.i_val==0?"iconst_0":"iconst_1");
            pushv("Lbool"); 
            pop1(sign_size);
            sign_size=0;
            expType="bool";
        }
#line 1707 "y.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 285 "compiler_hw3.y" /* yacc.c:1646  */
    {
            //printf("STRING_LIT %d\n",yylval.i_val);
            fprintf(fp,"ldc \"%s\"\n",yylval.s_val);
            pushv("Lstring"); 
            expType="string";
        }
#line 1718 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 295 "compiler_hw3.y" /* yacc.c:1646  */
    {
            popv();
            popv();
            pushv(ele);
            expType=ele;
            --ss_size;
            if(debug_mode==1) printf("\t[indExp]:--ss_size=%d\n",ss_size);
            if(sorl==1)
                fprintf(fp,"%caload\n",tolower(expType[0]));
        }
#line 1733 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 309 "compiler_hw3.y" /* yacc.c:1646  */
    {   
            //printf("%c to %c\n",(expType[0]-'a'+'A'),(ytype[0]-'a'+'A'));
                //For jasmin
            fprintf(fp,"%c2%c\n",tolower(expType[0]),tolower(ytype[0]));
            popv();
            pushv(ytype);
            expType=ytype;
            --ss_size;
            if(debug_mode==1) printf("\t(conExp):--ss_size=%d\n",ss_size);
        }
#line 1748 "y.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 339 "compiler_hw3.y" /* yacc.c:1646  */
    {
            if(strcmp(ytype,"int32")==0 && array==0)
                fprintf(fp,"ldc 0\n");
            else if(strcmp(ytype,"float32")==0 && array==0)
                fprintf(fp,"ldc 0.0\n");
            else if(strcmp(ytype,"string")==0 && array==0)
                fprintf(fp,"ldc \"\"\n");
            insert_symbol(id_name,scope_lev);
        }
#line 1762 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 349 "compiler_hw3.y" /* yacc.c:1646  */
    {
            insert_symbol(id_name,scope_lev);
            popv();
        }
#line 1771 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 361 "compiler_hw3.y" /* yacc.c:1646  */
    {
            if(check_symbol(yylval.s_val,scope_lev)==-1){
                printf("error:%d: undefined: %s\n",yylineno+1,yylval.s_val);
            }else if(sorl==1 || strcmp(type_symbol(yylval.s_val),"array")==0){
                //printf("IDENT (name=%s, address=%d)\n",yylval.s_val,addr_symbol(yylval.s_val));
                //For jasmin, load is needed
                char* ts=type_symbol(yylval.s_val);
                if(strcmp(ts,"int32")==0 || strcmp(ts,"bool")==0)
                    fprintf(fp,"iload %d\n",addr_symbol(yylval.s_val));
                else if(strcmp(ts,"float32")==0)
                    fprintf(fp,"fload %d\n",addr_symbol(yylval.s_val));
                else
                    fprintf(fp,"aload %d\n",addr_symbol(yylval.s_val));
            }
            expType=id_type=type_symbol(yylval.s_val);
            ele=ele_type_symbol(yylval.s_val);
            if(sorl==0) saddr=addr_symbol(yylval.s_val);
            pushv(expType);
        }
#line 1795 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 383 "compiler_hw3.y" /* yacc.c:1646  */
    {id_name=yylval.s_val;}
#line 1801 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 386 "compiler_hw3.y" /* yacc.c:1646  */
    {
                    sorl=1;
                    assign_id_type=expType;
                    if(strcmp(type_symbol(id_name),"array")==0) assign_array=1;
                }
#line 1811 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 390 "compiler_hw3.y" /* yacc.c:1646  */
    {sorl=0;pop1(1);}
#line 1817 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 395 "compiler_hw3.y" /* yacc.c:1646  */
    {push("ASSIGN");}
#line 1823 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 397 "compiler_hw3.y" /* yacc.c:1646  */
    {push("ADD_ASSIGN");}
#line 1829 "y.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 399 "compiler_hw3.y" /* yacc.c:1646  */
    {push("SUB_ASSIGN");}
#line 1835 "y.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 401 "compiler_hw3.y" /* yacc.c:1646  */
    {push("MUL_ASSIGN");}
#line 1841 "y.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 403 "compiler_hw3.y" /* yacc.c:1646  */
    {push("QUO_ASSIGN");}
#line 1847 "y.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 405 "compiler_hw3.y" /* yacc.c:1646  */
    {push("REM_ASSIGN");}
#line 1853 "y.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 411 "compiler_hw3.y" /* yacc.c:1646  */
    {
                    //printf("INC\n");
                    if(strcmp(expType,"int32")==0)
                        fprintf(fp,"iload %d\nldc 1\niadd\nistore %d\n",saddr,saddr);
                    else if(strcmp(expType,"float32")==0)
                        fprintf(fp,"fload %d\nldc 1.0\nfadd\nfstore %d\n",saddr,saddr);    
                }
#line 1865 "y.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 419 "compiler_hw3.y" /* yacc.c:1646  */
    {
                    //printf("DEC\n");
                    if(strcmp(expType,"int32")==0)
                        fprintf(fp,"iload %d\nldc 1\nisub\nistore %d\n",saddr,saddr);
                    else if(strcmp(expType,"float32")==0)
                        fprintf(fp,"fload %d\nldc 1.0\nfsub\nfstore %d\n",saddr,saddr);    
                }
#line 1877 "y.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 428 "compiler_hw3.y" /* yacc.c:1646  */
    {dump_symbol(scope_lev--);}
#line 1883 "y.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 432 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"L_if_%d_false:\n",ifNow[--ifLevel]);}
#line 1889 "y.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 434 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"L_if_%d_exit:\n",ifNow[--ifLevel]);}
#line 1895 "y.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 436 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"L_if_%d_exit:\n",ifNow[--ifLevel]);}
#line 1901 "y.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 439 "compiler_hw3.y" /* yacc.c:1646  */
    {sorl=1;ifNow[ifLevel++]=ifNo;ifNo++;}
#line 1907 "y.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 441 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"L_if_%d_false:\n",ifNow[ifLevel-1]);}
#line 1913 "y.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 443 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"ifeq L_if_%d_false\n",ifNow[ifLevel-1]);}
#line 1919 "y.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 447 "compiler_hw3.y" /* yacc.c:1646  */
    {
            sorl=0;
            if(strcmp(expType,"bool")!=0)
                printf("error:%d: non-bool (type %s) used as for condition\n",yylineno+1,expType);
        }
#line 1929 "y.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 454 "compiler_hw3.y" /* yacc.c:1646  */
    {
                    //fprintf(fp,"ifLevel=%d\n",ifLevel);
                    fprintf(fp,"goto L_if_%d_exit\n",ifNow[ifLevel-1]);
                }
#line 1938 "y.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 460 "compiler_hw3.y" /* yacc.c:1646  */
    {forLevel--;}
#line 1944 "y.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 461 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"label_%d_pre:\nifeq label_%d_end\n",forNow[forLevel-1],forNow[forLevel-1]);}
#line 1950 "y.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 461 "compiler_hw3.y" /* yacc.c:1646  */
    {forLevel--;}
#line 1956 "y.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 464 "compiler_hw3.y" /* yacc.c:1646  */
    {
            fprintf(fp,"for_label_%d_begin:\n",++forNo);
            forNow[forLevel++]=forNo;
            //sorl=1;
            ffor=1;
         }
#line 1967 "y.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 471 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"label_%d_begin:\n",forNow[forLevel-1]);}
#line 1973 "y.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 472 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"label_%d_post:\n",forNow[forLevel-1]);}
#line 1979 "y.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 473 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"goto label_%d_begin\n",forNow[forLevel-1]);}
#line 1985 "y.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 476 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"ifeq for_label_%d_end\n",forNow[forLevel-1]);}
#line 1991 "y.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 477 "compiler_hw3.y" /* yacc.c:1646  */
    {fprintf(fp,"goto label_%d_pre\n",forNow[forLevel-1]);}
#line 1997 "y.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 480 "compiler_hw3.y" /* yacc.c:1646  */
    {
                    fprintf(fp,"goto for_label_%d_begin\n",forNow[forLevel-1]);
                    fprintf(fp,"for_label_%d_end:\n",forNow[forLevel-1]);
                    ffor=0;
                 }
#line 2007 "y.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 486 "compiler_hw3.y" /* yacc.c:1646  */
    {
                    fprintf(fp,"goto label_%d_post\n",forNow[forLevel-1]);
                    fprintf(fp,"label_%d_end:\n",forNow[forLevel-1]);
                    ffor=0;
                  }
#line 2017 "y.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 493 "compiler_hw3.y" /* yacc.c:1646  */
    {sorl=1;}
#line 2023 "y.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 493 "compiler_hw3.y" /* yacc.c:1646  */
    {sorl=0;}
#line 2029 "y.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 494 "compiler_hw3.y" /* yacc.c:1646  */
    {
            if(strcmp(expType,"bool")==0){
                fprintf(fp,"ifeq L_cmp_%d\n",labelNo++);
                fprintf(fp,"ldc \"true\"\n");
                fprintf(fp,"goto L_cmp_%d\n",labelNo++);
                fprintf(fp,"L_cmp_%d:\n",labelNo-2);
                fprintf(fp,"ldc \"false\"\n");
                fprintf(fp,"L_cmp_%d:\n",labelNo-1);
                expType="string";
            }
            //printf("PRINT %s\n",expType); 
            fprintf(fp,"getstatic java/lang/System/out Ljava/io/PrintStream;\n");
            fprintf(fp,"swap\n");
            fprintf(fp,"invokevirtual java.io/PrintStream/print(");
            if(strcmp(expType,"int32")==0 || strcmp(expType,"float32")==0)
                fprintf(fp,"%c)V\n",expType[0]-32);
            else if(strcmp(expType,"string")==0)
                fprintf(fp,"Ljava/lang/String;)V\n");
            --ss_size;
            if(debug_mode==1) printf("\t(pf):--ss_size=%d\n",ss_size);
        }
#line 2055 "y.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 515 "compiler_hw3.y" /* yacc.c:1646  */
    {sorl=1;}
#line 2061 "y.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 515 "compiler_hw3.y" /* yacc.c:1646  */
    {sorl=0;}
#line 2067 "y.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 516 "compiler_hw3.y" /* yacc.c:1646  */
    {
            if(strcmp(expType,"bool")==0){
                fprintf(fp,"ifeq L_cmp_%d\n",labelNo++);
                fprintf(fp,"ldc \"true\"\n");
                fprintf(fp,"goto L_cmp_%d\n",labelNo++);
                fprintf(fp,"L_cmp_%d:\n",labelNo-2);
                fprintf(fp,"ldc \"false\"\n");
                fprintf(fp,"L_cmp_%d:\n",labelNo-1);
                expType="string";
            }
            //printf("PRINTLN %s\n",expType);
                //For jasmin println()
            fprintf(fp,"getstatic java/lang/System/out Ljava/io/PrintStream;\n");
            fprintf(fp,"swap\n");
            fprintf(fp,"invokevirtual java.io/PrintStream/println(");
            if(strcmp(expType,"int32")==0 || strcmp(expType,"float32")==0)
                fprintf(fp,"%c)V\n",expType[0]-32);
            else if(strcmp(expType,"string")==0)
                fprintf(fp,"Ljava/lang/String;)V\n");
            --ss_size;
            if(debug_mode==1) printf("\t(pfln):--ss_size=%d\n",ss_size);
        }
#line 2094 "y.tab.c" /* yacc.c:1646  */
    break;


#line 2098 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 539 "compiler_hw3.y" /* yacc.c:1906  */


/* C code section */
int main(int argc, char *argv[])
{
    if (argc == 2) {
        yyin = fopen(argv[1], "r");
    } else {
        yyin = stdin;
    }

    fp=fopen("hw3.j","w");
    

    yylineno = 0;
    fprintf(fp,".source hw3.j\n");
    fprintf(fp,".class public Main\n");
    fprintf(fp,".super java/lang/Object\n");
    fprintf(fp,".method public static main([Ljava/lang/String;)V\n");
    fprintf(fp,".limit stack 100\n");
    fprintf(fp,".limit locals 100\n");
    yyparse();
    //For jasmin, the next 2 lines is unnecessary for print out
    dump_symbol(0);
//	printf("Total lines: %d\n", yylineno);
    fprintf(fp,"return\n");
    fprintf(fp,".end method\n");
    fclose(yyin);
    fclose(fp);
    if(HAS_ERROR==1)
            remove("hw3.j");
    return 0;
}

static void create_symbol() {
}

static void insert_symbol(char* a,int scope_lev) {
    int temp=lookup_symbol(a,scope_lev);
    if(temp!=-1){
        printf("error:%d: %s redeclared in this block. previous declaration at line %d\n",yylineno,a,temp);
        HAS_ERROR=1;
        return;
    }
    //printf("> Insert {%s} into symbol table (scope level: %d)\n",a, scope_lev);
        //For jasmin, when declare, i/fstore is printed
    if(array==1){
        char* t;
        if(strcmp(ytype,"int32")==0)
            t="int";
        else if(strcmp(ytype,"float32")==0)
            t="float";
        fprintf(fp,"newarray %s\n",t);
    }
    if((strcmp(ytype,"int32")==0 || strcmp(ytype,"bool")==0)&&array==0)
        fprintf(fp,"istore %d\n",addr);
    else if(strcmp(ytype,"float32")==0 && array==0)
        fprintf(fp,"fstore %d\n",addr);
    else
        fprintf(fp,"astore %d\n",addr);
    
    symTable[ind].name=a;
    symTable[ind].type=(array==1?"array":ytype);
    symTable[ind].addr=addr++;
    symTable[ind].lineno=yylineno;
    symTable[ind].ele_type=(array==1?ytype:"-");
    symTable[ind++].scope=scope_lev;
    //printf("%d %s %s %d %d %s %d\n",ind-1,symTable[ind-1].name,symTable[ind-1].type, symTable[ind-1].addr, symTable[ind-1].lineno,symTable[ind-1].ele_type,symTable[ind-1].scope);
    array=0;
}

static int lookup_symbol(char* a,int scope_lev) {
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && symTable[i].scope==scope_lev)
            return symTable[i].lineno;     
    }
    return -1;
}

static int check_symbol(char* a,int scope_lev) {
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && symTable[i].scope<=scope_lev)
            return symTable[i].lineno;     
    }
    return -1;
}

static void dump_symbol(int sl) {
    //printf("> Dump symbol table (scope level: %d)\n", sl);
    //printf("%-10s%-10s%-10s%-10s%-10s%s\n",
//           "Index", "Name", "Type", "Address", "Lineno", "Element type");
    int j=0;
    //for(int i=0;i<ind;++i)
    //  if(symTable[i].scope==sl) printf("%-10d%-10s%-10s%-10d%-10d%s\n",
            //j++, symTable[i].name,symTable[i].type, symTable[i].addr, symTable[i].lineno,symTable[i].ele_type);
    ind-=j;
    if(debug_mode==1) printf("\t(dump_symbol):ind=%d\n",ind);
}

static int addr_symbol(char* a){
    int ans=-1;
    //checkTable(); 
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].addr;
    }
    return ans;    
}

static char* type_symbol(char* a){
    if(debug_mode==1) printf("Searching type of %s...\n",a);
    char* ans="undefined";
    for(int i=0;i<ind;++i){
        if(debug_mode==1) printf("\t(type_symbol):when i=%d\n",i);
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].type;
    }
    if (debug_mode==1) printf("Searched completed: %s\n",ans);
    return ans;    
}

static char* ele_type_symbol(char* a){
    char* ans="undefined";
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].ele_type;
    }
    return ans;    
}

static void push(char* s){
    if(debug_mode==1) printf("\t\t%s is pushed,ss_size=%d\n",s,ss_size);
    stack[stack_size++]=s;//stack store the op    
    ++s_stack[ss_size];//according to different level, stack size is difference and the accurate stack size is store in s_stack
}

static char* pop(){
   return stack[--stack_size];//the last op is pop out/print out    
}

static void pop1(int a){
   // printf("pop1(%d)\n",a);
    for(int i=0;i<a;++i){
       if(stack_size==0) break;//if nothing in the stack
       char* op=pop();
       char* t1=popv();//type
       if(strcmp(op,"POS")==0 || strcmp(op,"NEG")==0 || strcmp(op,"NOT")==0){//unary op
        if(strcmp(op,"NEG")==0)
            fprintf(fp,"%c%s\n",tolower(t1[0]=='L'?t1[1]:t1[0]),strlower(op));
        else if(strcmp(op,"NOT")==0)
            fprintf(fp,"iconst_1\nixor\n");
        pushv(t1);
        --s_stack[ss_size];
       }else{//binary op
        if(t1[0]=='L') t1=t1+1;
        char* t2=popv();
        if(strcmp(t1,"undefined")==0 || strcmp(t2,"undefined")==0){
            printf("%s\n", op);
            --s_stack[ss_size];
        }else if(t2[0]=='L'&&(strcmp(op,"ASSIGN")==0 || strcmp(op,"ADD_ASSIGN")==0||strcmp(op,"SUB_ASSIGN")==0||strcmp(op,"MUL_ASSIGN")==0 || strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0)){
            printf("error:%d: cannot assign to %s\n",yylineno,t2+1);
            printf("%s\n", op);
            --s_stack[ss_size];
            HAS_ERROR=1;
        }else{
            if(t2[0]=='L') t2=t2+1;
            if((strcmp(t1,"bool")!=0 && (strcmp(op,"LAND")==0 || strcmp(op,"LOR")==0)) || (strcmp(t1,"int32")!=0 && strcmp(op,"REM")==0)){
                printf("error:%d: invalid operation: (operator %s not defined on %s)\n",yylineno,op,t1);
                HAS_ERROR=1;
            }else if((strcmp(t2,"bool")!=0 && (strcmp(op,"LAND")==0 || strcmp(op,"LOR")==0)) || (strcmp(t2,"int32")!=0 && strcmp(op,"REM")==0)){
                printf("error:%d: invalid operation: (operator %s not defined on %s)\n",yylineno,op,t2);
                HAS_ERROR=1;
            }else if(strcmp(t1,t2)!=0){
                printf("error:%d: invalid operation: %s (mismatched types %s and %s)\n",yylineno,op,t2,t1);
                HAS_ERROR=1;
            }else{
                if(strcmp(op,"EQL")==0 || strcmp(op,"NEQ")==0 || strcmp(op,"LSS")==0 || strcmp(op,"LEQ")==0 || strcmp(op,"GTR")==0 || strcmp(op,"GEQ")==0){
                    pushv("bool");
                        //For jasmin
                    if(strcmp(t1,"int32")==0)
                        fprintf(fp,"isub\n");
                    else if(strcmp(t1,"float32")==0)
                        fprintf(fp,"fcmpl\n");
                    fprintf(fp,"if%c%c L_cmp_%d\n",tolower(op[0]),tolower(op[1]=='S'?'T':op[1]/*for LSS=>lt*/),labelNo++);
                    fprintf(fp,"iconst_0\n");
                    fprintf(fp,"goto L_cmp_%d\n",labelNo++);
                    fprintf(fp,"L_cmp_%d:\n",labelNo-2);
                    fprintf(fp,"iconst_1\n");
                    fprintf(fp,"L_cmp_%d:\n",labelNo-1);
                }else if(strcmp(op,"ASSIGN")==0 || strcmp(op,"ADD_ASSIGN")==0||strcmp(op,"SUB_ASSIGN")==0||strcmp(op,"MUL_ASSIGN")==0 || strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0){
                    char t=tolower(t1[0]);
                    if(t=='b') t='i';
                    if(t=='s') t='a';
                    if(assign_array==1){
                        fprintf(fp,"%castore\n",t);
                        assign_array=0;
                    }else{
                        if(strcmp(op,"ASSIGN")!=0){
                            fprintf(fp,"%cload %d\n",t,saddr);
                            if(strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"SUB_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0) fprintf(fp,"swap\n");
                            if(strcmp(op,"QUO_ASSIGN")!=0){
                                fprintf(fp,"%c%c%c%c\n",t,tolower(op[0]),tolower(op[1]),tolower(op[2]));
                            }
                            else
                                fprintf(fp,"%cdiv\n",t);
                        }
                        fprintf(fp,"%cstore %d\n",t,saddr);
                    }
                }else{
                    pushv(t1);
                    if(strcmp(op,"LAND")==0)
                        fprintf(fp,"iand\n");
                    else if(strcmp(op,"LOR")==0)
                        fprintf(fp,"ior\n");
                    else
                        fprintf(fp,"%c%s\n",tolower(t1[0]),strlower(op));
                }
            }
            //fprintf(fp,"%c%s\n",tolower(t1[0]),strlower(op));
            --s_stack[ss_size];
        }
       }
    }
}

static void popAll(){
   // printf("popAll()!\n");
    for(int i=stack_size-1;i>=0;--i)
        printf("%s\n",stack[i]);
    stack_size=0;    
}

static void checkTable(){
    for(int i=0;i<addr;++i) 
      printf("%-10d%-10s%-10s%-10d%-10d%s%-10d\n",i,symTable[i].name,symTable[i].type, symTable[i].addr, symTable[i].lineno,symTable[i].ele_type,symTable[i].scope);
           
}

static void pushv(char* s){
    stackv[stackv_size++]=s;    
    if(debug_mode==1) printf("\t(pushv):stackv_size++=%d,%s is pushed\n",stackv_size,s);
}

static char* popv(){
    if(debug_mode==1) printf("\t(popv):--stackv_size=%d,%s is pop!\n",stackv_size-1,stackv[stackv_size-1]);
    return stackv[--stackv_size];    
}

static char* strlower(char* a){
    if(debug_mode==1)
        printf("Processing %s...\n",a);
    char cpy[10];
    for(int i=0;i<strlen(a);++i){
        //printf("i=%d\n",i);
        cpy[i]=a[i]+32;//lower case the string a;
        if(debug_mode==1) printf("%s finished processed\n",cpy);
    }
    cpy[strlen(a)]='\0';
    a=cpy;
    return a;
}
