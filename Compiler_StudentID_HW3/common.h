#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct{
    char* name;
    char* type;
    int addr;
    int lineno;
    char* ele_type;    
    int scope;
}symbol;

#endif /* COMMON_H */
