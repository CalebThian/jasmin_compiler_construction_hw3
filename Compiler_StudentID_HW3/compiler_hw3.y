/*	Definition section */
%{
    #include "common.h" //Extern variables that communicate with lex
    #define MAXSTACK 1000
    // #define YYDEBUG 1
    // int yydebug = 1;

    extern int yylineno;
    extern int yylex();
    extern FILE *yyin;
    extern char* yytext; 
    void yyerror (char const *s)
    {
        printf("error:%d: %s\n", yylineno, s);
    }

    /* Symbol table function - you can add new function if needed. */
    static void create_symbol();
    static void insert_symbol(char* a,int scope_lev);
    static int lookup_symbol(char* a,int scope_lev);
    static int check_symbol(char* a,int scope_lev);
    static void dump_symbol();
    static int addr_symbol(char* a);
    static char* type_symbol(char* a);
    static char* ele_type_symbol(char* a);
    static void push(char* s);
    static char* pop();
    static void pop1(int a);
    static void popAll();
    static void checkTable();
    static void pushv(char*s);
    static char* popv();
    static char* strlower();

    /*global variable*/
    int ind=0;
    int addr=0;
    int scope_lev=0;
    symbol symTable[10000];
    char* ytype;
    int sign_size=0;
    int declare=0;
    char* stack[MAXSTACK];
    int s_stack[MAXSTACK];//scope of operator
    int ss_size;//temp of element of s_stack
    int stack_size=0;
    char* id_name;
    char* expType;
    int array=0;
    char* id_type;
    char* ele;
    int sorl=0;//store or load
    int saddr=0;//use for inc and dec
    int labelNo=0;//use for jump
    int ifNo=0;
    int ifNow[MAXSTACK];
    int ifLevel=0;
    int ffor=0;//flag of for
    int forNo=0;
    int forNow[MAXSTACK];
    int forLevel=0;
//    char* lastBool=0;//use for !
    char* assign_id_type;//assign-statement-use id
    int assign_array=0;//assign-statement-use array

    char* stackv[MAXSTACK];
    int stackv_size=0;
    int debug_mode=0;
    
    FILE *fp;
    int HAS_ERROR=0;
%}

%error-verbose

/* Use variable or self-defined structure to represent
 * nonterminal and token type
 */
%union {
    int i_val;
    float f_val;
    char *s_val;
    struct lr_val{
        char* l_val;
        char* r_val;
    }
    /* ... */
}

/* Token without return */
%token VAR
%token INT FLOAT BOOL STRING
%token INC DEC
%token GEQ LEQ EQL NEQ
%token ADD_ASSIGN SUB_ASSIGN MUL_ASSIGN QUO_ASSIGN REM_ASSIGN
%token LAND LOR
%token NEWLINE
%token PRINT
%token PRINTLN
%token IF ELSE FOR
%token QUOTA

/* Token with return, which need to specify type */
%token <i_val> INT_LIT
%token <f_val> FLOAT_LIT
%token <s_val> IDENT
%token <s_val> STRING_LIT
%token <i_val> BOOL_LIT

/* Nonterminal with return, which need to specify type */
/*%type <type> Type TypeName ArrayType*/

/* Yacc will start at this nonterminal */
%start Program


/* Grammar section */
%%

Program
    : StatementList
;

StatementList
    : StatementList Statement
    | Statement
;

Type
    : TypeName
    | ArrayType
;

TypeName
    : INT 
        {ytype="int32";}
    | FLOAT 
        {ytype="float32";}    
    | STRING 
        {ytype="string";} 
    | BOOL 
        {ytype="bool";}
;

ArrayType
    : '['Expression']' Type 
        {
            pop1(s_stack[ss_size]);
            --ss_size;
            if(debug_mode==1) printf("\t[arr_declare]:--ss_size=%d\n",ss_size);
            array=1;
        }
;

Expression
    : UnaryExpr
    | Expression LOR_op Term1 
        {pop1(s_stack[ss_size]);expType="bool";}
    | Term1
        {pop1(s_stack[ss_size]);}
;

Term1
    : UnaryExpr
    | Term1 LAND_op Term2
        {expType="bool";}
    | Term2
;

Term2
    : UnaryExpr
    | Term2 cmp_op{if(ffor==1) fprintf(fp,"%cload %d\n",tolower(expType[0]=='a'?ele[0]:expType[0]),saddr);} Term3
        {expType="bool";}
    | Term3
;   

Term3
    : UnaryExpr
    | Term3 add_op Term4
    | Term4
;

Term4
    : UnaryExpr
    | Term4 mul_op UnaryExpr {pop1(s_stack[ss_size]);}
;

UnaryExpr
    : PrimaryExpr
    | unary_op UnaryExpr
;


cmp_op 
    : EQL 
        {push("EQL");}
    | NEQ 
        {push("NEQ");}
    | '<' 
        {push("LSS");}
    | LEQ 
        {push("LEQ");}
    | '>' 
        {push("GTR");}
    | GEQ
        {push("GEQ");}
;

add_op
    : '+'
        {push("ADD");}
    |'-'
        {push("SUB");};

mul_op: '*' 
        {push("MUL");}
      |'/'
        {//push("QUO");
            //For jasmin, quo==>div
         push("DIV");
        }
      |'%'
        {push("REM");}
;

unary_op 
    : '+' 
        {push("POS");sign_size+=1;}
    | '-' 
        {push("NEG");sign_size+=1;}
    | '!'
        {push("NOT");sign_size+=1;}
;

LAND_op: LAND {push("LAND");};
LOR_op:  LOR {push("LOR");};

PrimaryExpr
    : Operand
    | IndexExpr
    | ConversionExpr
;

Operand
    : Literal
    | identifier
    | '('Expression')'
        {
            pop1(s_stack[ss_size]);
            --ss_size;
            if(debug_mode==1) printf("\t(exp):--ss_size=%d\n",ss_size);
        }
;

Literal
    : INT_LIT
        {
            //printf("INT_LIT %d\n",yylval.i_val);
            fprintf(fp,"ldc %d\n",yylval.i_val);
            pushv("Lint32");
            pop1(sign_size);
            sign_size=0;
            expType="int32";
        }
    | FLOAT_LIT
        {
            //printf("FLOAT_LIT %d\n",yylval.i_val);
            fprintf(fp,"ldc %lf\n",yylval.f_val);
            pushv("Lfloat32");
            pop1(sign_size);
            sign_size=0;
            expType="float32";
        }
    | BOOL_LIT
        {
            //printf("BOOL_LIT %d\n",yylval.i_val);
            fprintf(fp,"iconst_%d\n",yylval.i_val);
            //lastBool=(yylval.i_val==0?"iconst_0":"iconst_1");
            pushv("Lbool"); 
            pop1(sign_size);
            sign_size=0;
            expType="bool";
        }
    | QUOTA STRING_LIT QUOTA
        {
            //printf("STRING_LIT %d\n",yylval.i_val);
            fprintf(fp,"ldc \"%s\"\n",yylval.s_val);
            pushv("Lstring"); 
            expType="string";
        }
;

IndexExpr
    : PrimaryExpr '['Expression']' 
        {
            popv();
            popv();
            pushv(ele);
            expType=ele;
            --ss_size;
            if(debug_mode==1) printf("\t[indExp]:--ss_size=%d\n",ss_size);
            if(sorl==1)
                fprintf(fp,"%caload\n",tolower(expType[0]));
        }
;

ConversionExpr
    : Type '('Expression')' 
        {   
            //printf("%c to %c\n",(expType[0]-'a'+'A'),(ytype[0]-'a'+'A'));
                //For jasmin
            fprintf(fp,"%c2%c\n",tolower(expType[0]),tolower(ytype[0]));
            popv();
            pushv(ytype);
            expType=ytype;
            --ss_size;
            if(debug_mode==1) printf("\t(conExp):--ss_size=%d\n",ss_size);
        }
;

Statement
    : DeclarationStmt NEWLINE
    | SimpleStmt NEWLINE
    | Block NEWLINE
    | IfStmt NEWLINE
    | ForStmt NEWLINE
    | PrintStmt NEWLINE
    | NEWLINE
;

SimpleStmt
    : AssignmentStmt
    | ExpressionStmt
    | IncDecStmt
;

DeclarationStmt 
    :Declaration 
        {
            if(strcmp(ytype,"int32")==0 && array==0)
                fprintf(fp,"ldc 0\n");
            else if(strcmp(ytype,"float32")==0 && array==0)
                fprintf(fp,"ldc 0.0\n");
            else if(strcmp(ytype,"string")==0 && array==0)
                fprintf(fp,"ldc \"\"\n");
            insert_symbol(id_name,scope_lev);
        }
    | Declaration '=' Expression
        {
            insert_symbol(id_name,scope_lev);
            popv();
        }
;

Declaration
    : VAR identifier_dec Type
;

identifier
    : IDENT 
        {
            if(check_symbol(yylval.s_val,scope_lev)==-1){
                printf("error:%d: undefined: %s\n",yylineno+1,yylval.s_val);
            }else if(sorl==1 || strcmp(type_symbol(yylval.s_val),"array")==0){
                //printf("IDENT (name=%s, address=%d)\n",yylval.s_val,addr_symbol(yylval.s_val));
                //For jasmin, load is needed
                char* ts=type_symbol(yylval.s_val);
                if(strcmp(ts,"int32")==0 || strcmp(ts,"bool")==0)
                    fprintf(fp,"iload %d\n",addr_symbol(yylval.s_val));
                else if(strcmp(ts,"float32")==0)
                    fprintf(fp,"fload %d\n",addr_symbol(yylval.s_val));
                else
                    fprintf(fp,"aload %d\n",addr_symbol(yylval.s_val));
            }
            expType=id_type=type_symbol(yylval.s_val);
            ele=ele_type_symbol(yylval.s_val);
            if(sorl==0) saddr=addr_symbol(yylval.s_val);
            pushv(expType);
        }
;

identifier_dec
    : IDENT {id_name=yylval.s_val;};

AssignmentStmt
   : Expression{
                    sorl=1;
                    assign_id_type=expType;
                    if(strcmp(type_symbol(id_name),"array")==0) assign_array=1;
                } assign_op Expression {sorl=0;pop1(1);}
;

assign_op 
    : '='
        {push("ASSIGN");} 
    | ADD_ASSIGN 
        {push("ADD_ASSIGN");} 
    | SUB_ASSIGN 
        {push("SUB_ASSIGN");} 
    | MUL_ASSIGN 
        {push("MUL_ASSIGN");} 
    | QUO_ASSIGN 
        {push("QUO_ASSIGN");} 
    | REM_ASSIGN
        {push("REM_ASSIGN");} 
;

ExpressionStmt: Expression;

IncDecStmt : Expression INC
                {
                    //printf("INC\n");
                    if(strcmp(expType,"int32")==0)
                        fprintf(fp,"iload %d\nldc 1\niadd\nistore %d\n",saddr,saddr);
                    else if(strcmp(expType,"float32")==0)
                        fprintf(fp,"fload %d\nldc 1.0\nfadd\nfstore %d\n",saddr,saddr);    
                } 
           | Expression DEC
                {
                    //printf("DEC\n");
                    if(strcmp(expType,"int32")==0)
                        fprintf(fp,"iload %d\nldc 1\nisub\nistore %d\n",saddr,saddr);
                    else if(strcmp(expType,"float32")==0)
                        fprintf(fp,"fload %d\nldc 1.0\nfsub\nfstore %d\n",saddr,saddr);    
                }           
;

Block : '{'StatementList'}' {dump_symbol(scope_lev--);};

IfStmt
    : If Condition_if Block 
        {fprintf(fp,"L_if_%d_false:\n",ifNow[--ifLevel]);}
    | If Condition_if Block_if Else IfStmt 
        {fprintf(fp,"L_if_%d_exit:\n",ifNow[--ifLevel]);}
    | If Condition_if Block_if Else Block 
        {fprintf(fp,"L_if_%d_exit:\n",ifNow[--ifLevel]);}
;

If: IF {sorl=1;ifNow[ifLevel++]=ifNo;ifNo++;}

Else: ELSE{fprintf(fp,"L_if_%d_false:\n",ifNow[ifLevel-1]);}

Condition_if: Condition {fprintf(fp,"ifeq L_if_%d_false\n",ifNow[ifLevel-1]);}

Condition
    : Expression
        {
            sorl=0;
            if(strcmp(expType,"bool")!=0)
                printf("error:%d: non-bool (type %s) used as for condition\n",yylineno+1,expType);
        }    
;

Block_if: Block {
                    //fprintf(fp,"ifLevel=%d\n",ifLevel);
                    fprintf(fp,"goto L_if_%d_exit\n",ifNow[ifLevel-1]);
                }

ForStmt
    : For Condition_for Block_for {forLevel--;}
    | For ForClause {fprintf(fp,"label_%d_pre:\nifeq label_%d_end\n",forNow[forLevel-1],forNow[forLevel-1]);} Block_for_c {forLevel--;}
;

For: FOR {
            fprintf(fp,"for_label_%d_begin:\n",++forNo);
            forNow[forLevel++]=forNo;
            //sorl=1;
            ffor=1;
         }

ForClause: InitStmt ';'{fprintf(fp,"label_%d_begin:\n",forNow[forLevel-1]);} 
           Condition_for_c ';'{fprintf(fp,"label_%d_post:\n",forNow[forLevel-1]);} 
           PostStmt {fprintf(fp,"goto label_%d_begin\n",forNow[forLevel-1]);} ; 

InitStmt: SimpleStmt;
Condition_for: Condition {fprintf(fp,"ifeq for_label_%d_end\n",forNow[forLevel-1]);}//as no post inc/dec at next line of jasmin code
Condition_for_c: Condition {fprintf(fp,"goto label_%d_pre\n",forNow[forLevel-1]);}//as there is post inc/dec at the next line of jasmin code
PostStmt: SimpleStmt;

Block_for: Block {
                    fprintf(fp,"goto for_label_%d_begin\n",forNow[forLevel-1]);
                    fprintf(fp,"for_label_%d_end:\n",forNow[forLevel-1]);
                    ffor=0;
                 }

Block_for_c: Block{
                    fprintf(fp,"goto label_%d_post\n",forNow[forLevel-1]);
                    fprintf(fp,"label_%d_end:\n",forNow[forLevel-1]);
                    ffor=0;
                  }

PrintStmt
    : PRINT '(' {sorl=1;}Expression{sorl=0;} ')' 
        {
            if(strcmp(expType,"bool")==0){
                fprintf(fp,"ifeq L_cmp_%d\n",labelNo++);
                fprintf(fp,"ldc \"true\"\n");
                fprintf(fp,"goto L_cmp_%d\n",labelNo++);
                fprintf(fp,"L_cmp_%d:\n",labelNo-2);
                fprintf(fp,"ldc \"false\"\n");
                fprintf(fp,"L_cmp_%d:\n",labelNo-1);
                expType="string";
            }
            //printf("PRINT %s\n",expType); 
            fprintf(fp,"getstatic java/lang/System/out Ljava/io/PrintStream;\n");
            fprintf(fp,"swap\n");
            fprintf(fp,"invokevirtual java.io/PrintStream/print(");
            if(strcmp(expType,"int32")==0 || strcmp(expType,"float32")==0)
                fprintf(fp,"%c)V\n",expType[0]-32);
            else if(strcmp(expType,"string")==0)
                fprintf(fp,"Ljava/lang/String;)V\n");
            --ss_size;
            if(debug_mode==1) printf("\t(pf):--ss_size=%d\n",ss_size);
        }
    | PRINTLN '('{sorl=1;}Expression{sorl=0;}')' 
        {
            if(strcmp(expType,"bool")==0){
                fprintf(fp,"ifeq L_cmp_%d\n",labelNo++);
                fprintf(fp,"ldc \"true\"\n");
                fprintf(fp,"goto L_cmp_%d\n",labelNo++);
                fprintf(fp,"L_cmp_%d:\n",labelNo-2);
                fprintf(fp,"ldc \"false\"\n");
                fprintf(fp,"L_cmp_%d:\n",labelNo-1);
                expType="string";
            }
            //printf("PRINTLN %s\n",expType);
                //For jasmin println()
            fprintf(fp,"getstatic java/lang/System/out Ljava/io/PrintStream;\n");
            fprintf(fp,"swap\n");
            fprintf(fp,"invokevirtual java.io/PrintStream/println(");
            if(strcmp(expType,"int32")==0 || strcmp(expType,"float32")==0)
                fprintf(fp,"%c)V\n",expType[0]-32);
            else if(strcmp(expType,"string")==0)
                fprintf(fp,"Ljava/lang/String;)V\n");
            --ss_size;
            if(debug_mode==1) printf("\t(pfln):--ss_size=%d\n",ss_size);
        }
;
%%

/* C code section */
int main(int argc, char *argv[])
{
    if (argc == 2) {
        yyin = fopen(argv[1], "r");
    } else {
        yyin = stdin;
    }

    fp=fopen("hw3.j","w");
    

    yylineno = 0;
    fprintf(fp,".source hw3.j\n");
    fprintf(fp,".class public Main\n");
    fprintf(fp,".super java/lang/Object\n");
    fprintf(fp,".method public static main([Ljava/lang/String;)V\n");
    fprintf(fp,".limit stack 100\n");
    fprintf(fp,".limit locals 100\n");
    yyparse();
    //For jasmin, the next 2 lines is unnecessary for print out
    dump_symbol(0);
//	printf("Total lines: %d\n", yylineno);
    fprintf(fp,"return\n");
    fprintf(fp,".end method\n");
    fclose(yyin);
    fclose(fp);
    if(HAS_ERROR==1)
            remove("hw3.j");
    return 0;
}

static void create_symbol() {
}

static void insert_symbol(char* a,int scope_lev) {
    int temp=lookup_symbol(a,scope_lev);
    if(temp!=-1){
        printf("error:%d: %s redeclared in this block. previous declaration at line %d\n",yylineno,a,temp);
        HAS_ERROR=1;
        return;
    }
    //printf("> Insert {%s} into symbol table (scope level: %d)\n",a, scope_lev);
        //For jasmin, when declare, i/fstore is printed
    if(array==1){
        char* t;
        if(strcmp(ytype,"int32")==0)
            t="int";
        else if(strcmp(ytype,"float32")==0)
            t="float";
        fprintf(fp,"newarray %s\n",t);
    }
    if((strcmp(ytype,"int32")==0 || strcmp(ytype,"bool")==0)&&array==0)
        fprintf(fp,"istore %d\n",addr);
    else if(strcmp(ytype,"float32")==0 && array==0)
        fprintf(fp,"fstore %d\n",addr);
    else
        fprintf(fp,"astore %d\n",addr);
    
    symTable[ind].name=a;
    symTable[ind].type=(array==1?"array":ytype);
    symTable[ind].addr=addr++;
    symTable[ind].lineno=yylineno;
    symTable[ind].ele_type=(array==1?ytype:"-");
    symTable[ind++].scope=scope_lev;
    //printf("%d %s %s %d %d %s %d\n",ind-1,symTable[ind-1].name,symTable[ind-1].type, symTable[ind-1].addr, symTable[ind-1].lineno,symTable[ind-1].ele_type,symTable[ind-1].scope);
    array=0;
}

static int lookup_symbol(char* a,int scope_lev) {
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && symTable[i].scope==scope_lev)
            return symTable[i].lineno;     
    }
    return -1;
}

static int check_symbol(char* a,int scope_lev) {
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && symTable[i].scope<=scope_lev)
            return symTable[i].lineno;     
    }
    return -1;
}

static void dump_symbol(int sl) {
    //printf("> Dump symbol table (scope level: %d)\n", sl);
    //printf("%-10s%-10s%-10s%-10s%-10s%s\n",
//           "Index", "Name", "Type", "Address", "Lineno", "Element type");
    int j=0;
    //for(int i=0;i<ind;++i)
    //  if(symTable[i].scope==sl) printf("%-10d%-10s%-10s%-10d%-10d%s\n",
            //j++, symTable[i].name,symTable[i].type, symTable[i].addr, symTable[i].lineno,symTable[i].ele_type);
    ind-=j;
    if(debug_mode==1) printf("\t(dump_symbol):ind=%d\n",ind);
}

static int addr_symbol(char* a){
    int ans=-1;
    //checkTable(); 
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].addr;
    }
    return ans;    
}

static char* type_symbol(char* a){
    if(debug_mode==1) printf("Searching type of %s...\n",a);
    char* ans="undefined";
    for(int i=0;i<ind;++i){
        if(debug_mode==1) printf("\t(type_symbol):when i=%d\n",i);
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].type;
    }
    if (debug_mode==1) printf("Searched completed: %s\n",ans);
    return ans;    
}

static char* ele_type_symbol(char* a){
    char* ans="undefined";
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].ele_type;
    }
    return ans;    
}

static void push(char* s){
    if(debug_mode==1) printf("\t\t%s is pushed,ss_size=%d\n",s,ss_size);
    stack[stack_size++]=s;//stack store the op    
    ++s_stack[ss_size];//according to different level, stack size is difference and the accurate stack size is store in s_stack
}

static char* pop(){
   return stack[--stack_size];//the last op is pop out/print out    
}

static void pop1(int a){
   // printf("pop1(%d)\n",a);
    for(int i=0;i<a;++i){
       if(stack_size==0) break;//if nothing in the stack
       char* op=pop();
       char* t1=popv();//type
       if(strcmp(op,"POS")==0 || strcmp(op,"NEG")==0 || strcmp(op,"NOT")==0){//unary op
        if(strcmp(op,"NEG")==0)
            fprintf(fp,"%c%s\n",tolower(t1[0]=='L'?t1[1]:t1[0]),strlower(op));
        else if(strcmp(op,"NOT")==0)
            fprintf(fp,"iconst_1\nixor\n");
        pushv(t1);
        --s_stack[ss_size];
       }else{//binary op
        if(t1[0]=='L') t1=t1+1;
        char* t2=popv();
        if(strcmp(t1,"undefined")==0 || strcmp(t2,"undefined")==0){
            printf("%s\n", op);
            --s_stack[ss_size];
        }else if(t2[0]=='L'&&(strcmp(op,"ASSIGN")==0 || strcmp(op,"ADD_ASSIGN")==0||strcmp(op,"SUB_ASSIGN")==0||strcmp(op,"MUL_ASSIGN")==0 || strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0)){
            printf("error:%d: cannot assign to %s\n",yylineno,t2+1);
            printf("%s\n", op);
            --s_stack[ss_size];
            HAS_ERROR=1;
        }else{
            if(t2[0]=='L') t2=t2+1;
            if((strcmp(t1,"bool")!=0 && (strcmp(op,"LAND")==0 || strcmp(op,"LOR")==0)) || (strcmp(t1,"int32")!=0 && strcmp(op,"REM")==0)){
                printf("error:%d: invalid operation: (operator %s not defined on %s)\n",yylineno,op,t1);
                HAS_ERROR=1;
            }else if((strcmp(t2,"bool")!=0 && (strcmp(op,"LAND")==0 || strcmp(op,"LOR")==0)) || (strcmp(t2,"int32")!=0 && strcmp(op,"REM")==0)){
                printf("error:%d: invalid operation: (operator %s not defined on %s)\n",yylineno,op,t2);
                HAS_ERROR=1;
            }else if(strcmp(t1,t2)!=0){
                printf("error:%d: invalid operation: %s (mismatched types %s and %s)\n",yylineno,op,t2,t1);
                HAS_ERROR=1;
            }else{
                if(strcmp(op,"EQL")==0 || strcmp(op,"NEQ")==0 || strcmp(op,"LSS")==0 || strcmp(op,"LEQ")==0 || strcmp(op,"GTR")==0 || strcmp(op,"GEQ")==0){
                    pushv("bool");
                        //For jasmin
                    if(strcmp(t1,"int32")==0)
                        fprintf(fp,"isub\n");
                    else if(strcmp(t1,"float32")==0)
                        fprintf(fp,"fcmpl\n");
                    fprintf(fp,"if%c%c L_cmp_%d\n",tolower(op[0]),tolower(op[1]=='S'?'T':op[1]/*for LSS=>lt*/),labelNo++);
                    fprintf(fp,"iconst_0\n");
                    fprintf(fp,"goto L_cmp_%d\n",labelNo++);
                    fprintf(fp,"L_cmp_%d:\n",labelNo-2);
                    fprintf(fp,"iconst_1\n");
                    fprintf(fp,"L_cmp_%d:\n",labelNo-1);
                }else if(strcmp(op,"ASSIGN")==0 || strcmp(op,"ADD_ASSIGN")==0||strcmp(op,"SUB_ASSIGN")==0||strcmp(op,"MUL_ASSIGN")==0 || strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0){
                    char t=tolower(t1[0]);
                    if(t=='b') t='i';
                    if(t=='s') t='a';
                    if(assign_array==1){
                        fprintf(fp,"%castore\n",t);
                        assign_array=0;
                    }else{
                        if(strcmp(op,"ASSIGN")!=0){
                            fprintf(fp,"%cload %d\n",t,saddr);
                            if(strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"SUB_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0) fprintf(fp,"swap\n");
                            if(strcmp(op,"QUO_ASSIGN")!=0){
                                fprintf(fp,"%c%c%c%c\n",t,tolower(op[0]),tolower(op[1]),tolower(op[2]));
                            }
                            else
                                fprintf(fp,"%cdiv\n",t);
                        }
                        fprintf(fp,"%cstore %d\n",t,saddr);
                    }
                }else{
                    pushv(t1);
                    if(strcmp(op,"LAND")==0)
                        fprintf(fp,"iand\n");
                    else if(strcmp(op,"LOR")==0)
                        fprintf(fp,"ior\n");
                    else
                        fprintf(fp,"%c%s\n",tolower(t1[0]),strlower(op));
                }
            }
            //fprintf(fp,"%c%s\n",tolower(t1[0]),strlower(op));
            --s_stack[ss_size];
        }
       }
    }
}

static void popAll(){
   // printf("popAll()!\n");
    for(int i=stack_size-1;i>=0;--i)
        printf("%s\n",stack[i]);
    stack_size=0;    
}

static void checkTable(){
    for(int i=0;i<addr;++i) 
      printf("%-10d%-10s%-10s%-10d%-10d%s%-10d\n",i,symTable[i].name,symTable[i].type, symTable[i].addr, symTable[i].lineno,symTable[i].ele_type,symTable[i].scope);
           
}

static void pushv(char* s){
    stackv[stackv_size++]=s;    
    if(debug_mode==1) printf("\t(pushv):stackv_size++=%d,%s is pushed\n",stackv_size,s);
}

static char* popv(){
    if(debug_mode==1) printf("\t(popv):--stackv_size=%d,%s is pop!\n",stackv_size-1,stackv[stackv_size-1]);
    return stackv[--stackv_size];    
}

static char* strlower(char* a){
    if(debug_mode==1)
        printf("Processing %s...\n",a);
    char cpy[10];
    for(int i=0;i<strlen(a);++i){
        //printf("i=%d\n",i);
        cpy[i]=a[i]+32;//lower case the string a;
        if(debug_mode==1) printf("%s finished processed\n",cpy);
    }
    cpy[strlen(a)]='\0';
    a=cpy;
    return a;
}
