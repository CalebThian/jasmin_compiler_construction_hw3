.source hw3.j
.class public Main
.super java/lang/Object
.method public static main([Ljava/lang/String;)V
.limit stack 100
.limit locals 100
ldc 0
istore 0
for_label_1_begin:
ldc 0
istore 0
label_1_begin:
iload 0
ldc 10
isub
iflt L_cmp_0
iconst_0
goto L_cmp_1
L_cmp_0:
iconst_1
L_cmp_1:
goto label_1_pre
label_1_post:
iload 0
ldc 1
iadd
istore 0
goto label_1_begin
label_1_pre:
ifeq label_1_end
iload 0
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
goto label_1_post
label_1_end:
ldc 3
newarray int
astore 1
aload 1
ldc 0
ldc 1
ldc 2
iadd
iastore
aload 1
ldc 1
aload 1
ldc 0
iaload
ldc 1
isub
iastore
aload 1
ldc 2
aload 1
ldc 1
iaload
ldc 3
idiv
iastore
aload 1
ldc 2
iaload
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
ldc 3
ldc 4
ldc 5
ldc 8
ineg
iadd
imul
isub
ldc 10
ldc 7
idiv
isub
ldc 4
ineg
ldc 3
irem
isub
ifgt L_cmp_2
iconst_0
goto L_cmp_3
L_cmp_2:
iconst_1
L_cmp_3:
iconst_1
iconst_1
ixor
iconst_0
iconst_1
ixor
iconst_1
ixor
iand
ior
ifeq L_cmp_4
ldc "true"
goto L_cmp_5
L_cmp_4:
ldc "false"
L_cmp_5:
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(Ljava/lang/String;)V
ldc 3
newarray float
astore 2
aload 2
ldc 0
ldc 1.100000
ldc 2.100000
fadd
fastore
aload 2
ldc 0
faload
f2i
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
ldc 0
istore 3
ldc 2
iload 3
iadd
istore 3
for_label_2_begin:
iload 3
ldc 0
isub
ifgt L_cmp_6
iconst_0
goto L_cmp_7
L_cmp_6:
iconst_1
L_cmp_7:
ifeq for_label_2_end
iload 3
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
iload 3
ldc 1
isub
istore 3
iload 3
ldc 0
isub
ifne L_cmp_8
iconst_0
goto L_cmp_9
L_cmp_8:
iconst_1
L_cmp_9:
ifeq L_if_0_false
ldc 3.140000
fstore 4
fload 4
ldc 1.000000
fadd
f2i
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
ldc "If x != "
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(Ljava/lang/String;)V
ldc 0
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
fload 4
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(F)V
goto L_if_0_exit
L_if_0_false:
ldc 6.600000
fstore 5
ldc "If x == "
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(Ljava/lang/String;)V
ldc 0
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(I)V
fload 5
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/println(F)V
L_if_0_exit:
ldc 0
istore 6
for_label_3_begin:
ldc 1
istore 6
label_3_begin:
iload 6
ldc 3
isub
ifle L_cmp_10
iconst_0
goto L_cmp_11
L_cmp_10:
iconst_1
L_cmp_11:
goto label_3_pre
label_3_post:
iload 6
ldc 1
iadd
istore 6
goto label_3_begin
label_3_pre:
ifeq label_3_end
iload 3
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(I)V
ldc "*"
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(Ljava/lang/String;)V
iload 6
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(I)V
ldc "="
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(Ljava/lang/String;)V
iload 3
iload 6
imul
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(I)V
ldc "\t"
getstatic java/lang/System/out Ljava/io/PrintStream;
swap
invokevirtual java.io/PrintStream/print(Ljava/lang/String;)V
goto label_3_post
label_3_end:
goto for_label_2_begin
for_label_2_end:
return
.end method
